import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: ''),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController celsiusContrler = new TextEditingController();
  TextEditingController fahrenheitContrler = new TextEditingController();
 
  @override
  initState() {
   // initController();
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    celsiusContrler.dispose();
    fahrenheitContrler.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.all(20.0),
              width: double.infinity,
              child: Theme(
                data: new ThemeData(primaryColor: Colors.grey),
                child: TextField(
                  controller: celsiusContrler,
                  keyboardType: TextInputType.number,
                  onChanged: (value){
                    setState(() {
                      fahrenheitContrler.text = ((double.parse(value) * 1.8)+ 32.0).toString();
                    });
                  },
                  decoration: InputDecoration(
                      labelText: 'Celsius C',
                      labelStyle: new TextStyle(
                          fontSize: 17.0,
                          color: Colors.black),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.zero,
                          borderSide: BorderSide(
                              color: Colors.green, width: 1.5)),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.zero,
                      )),
                ),
              ),
            ),
             new Container(
              margin: const EdgeInsets.all(20.0),
              width: double.infinity,
              child: Theme(
                data: new ThemeData(primaryColor: Colors.grey),
                child: TextField(
                  controller: fahrenheitContrler,
                  onChanged: (value){
                    setState(() {
                      celsiusContrler.text = ((double.parse(value) - 32.0)/1.8).toStringAsFixed(1);
                    });
                  },
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: 'Fahrenheit F',
                      labelStyle: new TextStyle(
                          fontSize: 17.0,
                          color: Colors.black),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.zero,
                          borderSide: BorderSide(
                              color: Colors.green, width: 1.5)),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.zero,
                      )),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
